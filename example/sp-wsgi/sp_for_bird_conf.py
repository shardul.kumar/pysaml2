'''
This file is for generating SP metadata for BIRD platform. 
To generate the same run as below in the current folder.

     ../../tools/make_metadata.py sp_for_bird_conf.py > bird_sp.xml   
'''
from saml2.entity_category.edugain import COC
from saml2 import BINDING_HTTP_REDIRECT
from saml2 import BINDING_HTTP_POST
from saml2.saml import NAME_FORMAT_URI
import requests

try:
    from saml2.sigver import get_xmlsec_binary
except ImportError:
    get_xmlsec_binary = None


if get_xmlsec_binary:
    xmlsec_path = get_xmlsec_binary(["/opt/local/bin","/usr/local/bin"])
else:
    xmlsec_path = '/usr/local/bin/xmlsec1'

BASE = "http://inbp-dev.nova.cognostics.de"
rv = requests.get('https://saml-bird.daad.com/saml2/idp/metadata.php')

CONFIG = {
    'entityid': "https://inbp-dev.nova.cognostics.de/sp.xml",
    'metadata': {'inline': [rv.text]},                # download metadata file of counterpart entity (ie IDP) using http get and read its content
    #'metadata': {"local": ["../idp2/idp.xml"]},      # or metadata file of counterpart entity (ie IDP) is saved locally
    'service': {
        'sp': {
            'endpoints': {
                'assertion_consumer_service': [
                    ("http://inbp-dev.nova.cognostics.de/api/saml/sso/okta", BINDING_HTTP_REDIRECT),
                    ("http://inbp-dev.nova.cognostics.de/api/saml/sso/okta", BINDING_HTTP_POST),
                    ("https://inbp-dev.nova.cognostics.de/api/saml/sso/okta", BINDING_HTTP_REDIRECT),
                    ("https://inbp-dev.nova.cognostics.de/api/saml/sso/okta", BINDING_HTTP_POST)
                ],
            },
            # Don't verify that the incoming requests originate from us via
            # the built-in cache for authn request ids in pysaml2
            'allow_unsolicited': True,
            # Don't sign authn requests, since signed requests only make
            # sense in a situation where you control both the SP and IdP
            'authn_requests_signed': False,
            'logout_requests_signed': True,
            'want_assertions_signed': True,
            'want_response_signed': False,
        },
    },
    # This adds the content of key file in the genrated metadata.
    "key_file": "pki/mykey.pem",
    # This adds the content of certificate file in the genrated metadata.
    "cert_file": "pki/mycert.pem",
    "xmlsec_binary": xmlsec_path,
    "name_form": NAME_FORMAT_URI,
}
